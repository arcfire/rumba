#!/usr/bin/env python

import argparse
import csv
import math

description = "Subtracts two CSV from each other"
epilog = "2018 Sander Vrijders <sander.vrijders@ugent.be>"

parser = argparse.ArgumentParser(description=description,
                                 epilog=epilog)

parser.add_argument('-o', '--output', type=str, required=True,
                    help='Output CSV filename.')

parser.add_argument('-f', '--first', type=str, required=True,
                    help='First input CSV filename.')

parser.add_argument('-s', '--second', type=str, required=True,
                    help='Second input CSV filename.')

args = parser.parse_args()

csvfile = open(args.second, 'r')
reader = csv.reader(csvfile, delimiter=';', quotechar='|')

# Set second measurement
bandwidth = {}
for row in reader:
    if (row[1], row[0]) in bandwidth:
        bandwidth[row[1], row[0]] += int(row[2])
    else:
        bandwidth[row[0], row[1]] = int(row[2])

# Subtract first measurement
csvfile2 = open(args.first, 'r')
reader2 = csv.reader(csvfile2, delimiter=';', quotechar='|')
for row in reader2:
    if (row[1], row[0]) in bandwidth:
        bandwidth[row[1], row[0]] -= int(row[2])
    else:
        bandwidth[row[0], row[1]] -= int(row[2])


csvfile3 = open(args.output, 'w')
writer = csv.writer(csvfile3, delimiter=';', quotechar='|')
for key, value in bandwidth.items():
    writer.writerow([key[0], key[1], value])
