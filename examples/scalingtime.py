#!/usr/bin/env python

from rumba.model import *
from rumba.utils import *

# import testbed plugins
import rumba.testbeds.emulab as emulab
import rumba.testbeds.jfed as jfed
import rumba.testbeds.qemu as qemu
import rumba.testbeds.local as local
import rumba.testbeds.dockertb as docker

# import prototype plugins
import rumba.prototypes.ouroboros as our
import rumba.prototypes.rlite as rl
import rumba.prototypes.irati as irati

import argparse
import sys


description = "Script to create the demo from the Rumba paper"

argparser = argparse.ArgumentParser(description = description)

# Parameters to set the experiment size
argparser.add_argument('--metro-networks', type = int, default = '2',
                       help = "Number of metro networks")
argparser.add_argument('--metro-nodes', type = int, default = '5',
                       help = "Number of nodes per metro")
argparser.add_argument('--end-nodes', type = int, default = '2',
                       help = "Number of nodes connected to each metro")

# Other parameters
argparser.add_argument('--verbosity', metavar='VERBOSITY', type=str,
                       default='INFO',
                       choices=['DEBUG', 'INFO', 'WARNING',
                                'ERROR', 'CRITICAL'],
                       help='Logging verbosity level')
argparser.add_argument('--prototype', type=str, required=True,
                       choices=['irati', 'ouroboros', 'rlite'],
                       help='The kind of prototype plugin to use to run'
                       ' the experiment.')

subparsers = argparser.add_subparsers(dest='testbed')
emulab_p = subparsers.add_parser('emulab', help='Use emulab testbed')
jfed_p = subparsers.add_parser('jfed', help='Use jfed testbed')
qemu_p = subparsers.add_parser('qemu', help='Use qemu testbed')
local_p = subparsers.add_parser('local', help='Use local testbed')
docker_p = subparsers.add_parser('docker', help='Use Docker testbed')

qemu_p.add_argument('--bzimage', metavar='BZIMAGE', type=str,
                    help='path to the bzImage file to use')
qemu_p.add_argument('--initramfs', metavar='INITRAMFS', type=str,
                    help='path to the initramfs file to use')
qemu_p.add_argument('--use_vhost', action='store_true',
                    default=False, help='Use vhost')
qemu_p.add_argument('--qemu_logs_dir', metavar='QEMU_LOGS', type=str,
                    default=None, help='path to the folder for qemu logs')
qemu_p.add_argument('--username', type = str, help = "Username")
qemu_p.add_argument('--password', type = str, help = "Password")

emulab_p.add_argument('--url', metavar='URL', type=str,
                      default="wall2.ilabt.iminds.be",
                      help='Url')
emulab_p.add_argument('--image', metavar='IMG', type=str,
                      default="UBUNTU14-64-STD",
                      help='Ubuntu image')
emulab_p.add_argument('--username', type = str, required=True,
                      help = "Username")
emulab_p.add_argument('--password', type = str, help = "Password")
emulab_p.add_argument('--exp-name', type = str, required=True,
                      help = "Name of the experiment")

jfed_p.add_argument('--cert-file', type = str,
                    required=True,
                    help = "Absolute path to certificate (.pem) file"
                    " to be used with jFed")
jfed_p.add_argument('--authority', type = str, default = 'exogeni.net',
                    help = "jFed testbed to use")
jfed_p.add_argument('--image', metavar='IMAGE', type=str,
                    default=None,
                    help='Image to be used')
jfed_p.add_argument('--exp-hours', metavar='HOURS', type=str,
                    default="2", help='Experiment hours')
jfed_p.add_argument('--username', type = str, required=True,
                    help = "Username")
jfed_p.add_argument('--exp-name', type = str, required=True,
                    help = "Name of the experiment")

docker_p.add_argument('--base-image', metavar='DOCKIMAGE', type=str,
                      default=None, required=True,
                      help='Docker image to be used')
docker_p.add_argument('--use-ovs', action='store_true',
                      default=False, help='Use ovs switch')

args = argparser.parse_args()

log.set_logging_level(args.verbosity)

pi = NormalDIF("pi")
pi.dt_strategy = 'minimal'

c = NormalDIF("core")

nodes = []

core_nodes = []

for i in range(0, args.metro_networks):
    m = NormalDIF("m-" + str(i))
    e_prev = None
    m1 = None
    mn = None
    for j in range(0, args.metro_nodes):
        if j is not (args.metro_nodes - 1):
            e = ShimEthDIF("e-" + str(i) + "-" + str(j))
        else:
            e = None
        if e_prev == None and e != None:
            node = Node("metro-" + str(i) + "-" + str(j),
                        difs = [m, pi, e],
                        dif_registrations = {pi : [m], m : [e]})
            m1 = node
        elif e_prev != None and e != None:
            node = Node("metro-" + str(i) + "-" + str(j),
                        difs = [m, pi, e, e_prev],
                        dif_registrations = {pi : [m], m : [e, e_prev]})
        else:
            node = Node("metro-" + str(i) + "-" + str(j),
                        difs = [m, pi, e_prev],
                        dif_registrations = {pi : [m], m : [e_prev]})
            mn = node
        nodes.append(node)
        e_prev = e
        # Create nodes attached to metro
        for k in range(0, args.end_nodes):
            ee = ShimEthDIF("e-" + str(i) + "-" + str(j) + "-" + str(k))
            end_node = Node("end-" + str(i) + "-" + str(j) + "-" + str(k),
                            difs = [pi, ee],
                            dif_registrations = {pi : [ee]})
            node.add_dif(ee)
            node.add_dif_registration(pi, ee)
            nodes.append(end_node)

    e = ShimEthDIF("e-" + str(i) + "-" + str(args.metro_nodes))
    m1.add_dif(e)
    m1.add_dif_registration(m, e)
    mn.add_dif(e)
    mn.add_dif_registration(m, e)

    # Add 2 links from metro to core
    e0 = ShimEthDIF("c-e-" + str(i) + "-0")
    e1 = ShimEthDIF("c-e-" + str(i) + "-1")
    e2 = ShimEthDIF("c-e-" + str(i) + "-2")

    c1 = Node("c-" + str(i) + "-0",
              difs = [c, m, pi, e0, e2],
              dif_registrations = {pi : [m, c], m : [e0], c : [e2]})

    c2 = Node("c-" + str(i) + "-1",
              difs = [c, m, pi, e1, e2],
              dif_registrations = {pi : [m, c], m : [e1], c : [e2]})

    nodes.append(c1)
    nodes.append(c2)

    m1.add_dif(e0)
    m1.add_dif_registration(m, e0)
    mn.add_dif(e1)
    mn.add_dif_registration(m, e1)

    for x in range(0, len(core_nodes), 2):
        ce0 = ShimEthDIF("c-c-e-" + str(i) + "-" + str(x))
        ce1 = ShimEthDIF("c-c-e-" + str(i) + "-" + str(x + 1))

        core_nodes[x].add_dif(ce0)
        core_nodes[x].add_dif_registration(c, ce0)
        core_nodes[x + 1].add_dif(ce1)
        core_nodes[x + 1].add_dif_registration(c, ce1)

        c1.add_dif(ce0)
        c1.add_dif_registration(c, ce0)
        c2.add_dif(ce1)
        c2.add_dif_registration(c, ce1)

    core_nodes.append(c1)
    core_nodes.append(c2)

if args.testbed == 'emulab':
    test_class = emulab.Testbed
    testbed_args = {a.dest: getattr(args, a.dest)
                    for a in emulab_p._actions
                    if a.dest != 'help'
                    and getattr(args, a.dest) is not None}
elif args.testbed == 'jfed':
    test_class = jfed.Testbed
    testbed_args = {a.dest: getattr(args, a.dest)
                    for a in jfed_p._actions
                    if a.dest != 'help'
                    and getattr(args, a.dest) is not None}
elif args.testbed == 'qemu':
    test_class = qemu.Testbed
    testbed_args = {a.dest: getattr(args, a.dest)
                    for a in qemu_p._actions
                    if a.dest != 'help'
                    and getattr(args, a.dest) is not None}
elif args.testbed == 'local':
    test_class = local.Testbed
    testbed_args = {a.dest: getattr(args, a.dest)
                    for a in local_p._actions
                    if a.dest != 'help'
                    and getattr(args, a.dest) is not None}
elif args.testbed == 'docker':
    test_class = docker.Testbed
    testbed_args = {a.dest: getattr(args, a.dest)
                    for a in docker_p._actions
                    if a.dest != 'help'
                    and getattr(args, a.dest) is not None}
elif args.testbed is None:
    print('Testbed type must be specified!')
    print(argparser.format_help())
    exit(1)

testbed = test_class(**testbed_args)

if args.prototype == 'irati':
    exp_class = irati.Experiment
elif args.prototype == 'ouroboros':
    exp_class = our.Experiment
elif args.prototype == 'rlite':
    exp_class = rl.Experiment
else:
    print('Unexpected prototype!')
    print(argparser.format_help())
    exit(1)

exp = exp_class(testbed, nodes = nodes)

with ExperimentManager(exp, swap_out_strategy=PAUSE_SWAPOUT):
    exp.swap_in()
    if not isinstance(testbed, docker.Testbed) \
       and not isinstance(testbed, qemu.Testbed) \
       and not isinstance(testbed, local.Testbed):
        exp.install_prototype()
    exp.bootstrap_prototype()
