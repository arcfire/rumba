#!/usr/bin/env python

from rumba.model import *
from rumba.utils import ExperimentManager

import rumba.testbeds.jfed as jfed
import rumba.prototypes.rlite as rlite

import rumba.log as log

import argparse


description = "Script to run rlite on jfed"
epilog = "2017 H2020 ARCFIRE"

argparser = argparse.ArgumentParser(description = description,
                                    epilog = epilog)
argparser.add_argument('--user', type = str, default = 'vmaffio',
                       help = "jFed username")
argparser.add_argument('--cert', type = str,
                       help = "Absolute path to certificate (.pem) file"
                            " to be used with jFed",
                        default = '/home/vmaffione/Downloads/vmaffio-jfed.pem')
argparser.add_argument('--expname', type = str, default = 'pinocchio',
                       help = "Name of the experiment within the jFed testbed")

args = argparser.parse_args()

log.set_logging_level('DEBUG')

n1 = NormalDIF("n1")

e1 = ShimEthDIF("e1")

a = Node("a",
         difs = [n1, e1],
         dif_registrations = {n1 : [e1]})

b = Node("b",
         difs = [e1, n1],
         dif_registrations = {n1 : [e1]})

tb = jfed.Testbed(exp_name = args.expname,
                  cert_file = args.cert,
                  username = args.user)

exp = rlite.Experiment(tb, nodes = [a, b])

with ExperimentManager(exp):
    exp.swap_in()
    exp.install_prototype()
    exp.bootstrap_prototype()
