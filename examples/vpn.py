#!/usr/bin/env python

# An example script using the rumba package

from rumba.model import *
from rumba.utils import ExperimentManager

# import testbed plugins
import rumba.testbeds.emulab as emulab
import rumba.testbeds.jfed as jfed
import rumba.testbeds.local as local
import rumba.testbeds.qemu as qemu

# import prototype plugins
import rumba.prototypes.ouroboros as our
import rumba.prototypes.rlite as rl
import rumba.prototypes.irati as irati

import rumba.log as log

log.set_logging_level('DEBUG')


n1 = NormalDIF("n1")
n2 = NormalDIF("n2")
e1 = ShimEthDIF("e1")
e2 = ShimEthDIF("e2")
e3 = ShimEthDIF("e3")

a = Node("a",
         difs = [e1, n1, n2],
         dif_registrations = {n1 : [e1], n2 : [n1]})

b = Node("b",
         difs = [e1, e2, n1],
         dif_registrations = {n1 : [e1, e2]})

c = Node("c",
         difs = [e2, e3, n1],
         dif_registrations = {n1 : [e2, e3]})

d = Node("d",
         difs = [e3, n1, n2],
         dif_registrations = {n1 : [e3], n2 : [n1]})

tb = qemu.Testbed(exp_name = 'example1',
                  username = 'root',
                  password = 'root')

exp = our.Experiment(tb, nodes = [a, b, c, d])

print(exp)

# with ExperimentManager(exp):
#    exp.swap_in()
#    exp.bootstrap_prototype()
