#!/usr/bin/env python

# An example script using the rumba package
from rumba.model import *

# import testbed plugins
import rumba.testbeds.emulab as emulab
import rumba.testbeds.jfed as jfed
import rumba.testbeds.local as local
import rumba.testbeds.qemu as qemu
import rumba.testbeds.dockertb as docker

# import prototype plugins
import rumba.prototypes.ouroboros as our
import rumba.prototypes.rlite as rl
import rumba.prototypes.irati as irati

import rumba.log as log

from rumba.storyboard import *

from rumba.utils import *

import argparse
import time

log.set_logging_level('DEBUG')

n1 = NormalDIF("n1")

n1.add_policy("routing", "lfa")
n1.add_policy("pff", "alternate")

e1 = ShimEthDIF("e1")
e2 = ShimEthDIF("e2")
e3 = ShimEthDIF("e3")
e4 = ShimEthDIF("e4")
e5 = ShimEthDIF("e5")
e6 = ShimEthDIF("e6")
e7 = ShimEthDIF("e7")
e8 = ShimEthDIF("e8")
e9 = ShimEthDIF("e9")
e10 = ShimEthDIF("e10")
e11 = ShimEthDIF("e11")
e12 = ShimEthDIF("e12")
e13 = ShimEthDIF("e13")
e14 = ShimEthDIF("e14")
e15 = ShimEthDIF("e15")
e16 = ShimEthDIF("e16")
e17 = ShimEthDIF("e17")
e18 = ShimEthDIF("e18")
e19 = ShimEthDIF("e19")
e20 = ShimEthDIF("e20")
e21 = ShimEthDIF("e21")
e22 = ShimEthDIF("e22")
e23 = ShimEthDIF("e23")
e24 = ShimEthDIF("e24")
e25 = ShimEthDIF("e25")
e26 = ShimEthDIF("e26")
e27 = ShimEthDIF("e27")
e28 = ShimEthDIF("e28")
e29 = ShimEthDIF("e29")
e30 = ShimEthDIF("e30")
e31 = ShimEthDIF("e31")
e32 = ShimEthDIF("e32")
e33 = ShimEthDIF("e33")
e34 = ShimEthDIF("e34")
e35 = ShimEthDIF("e35")
e36 = ShimEthDIF("e36")
e37 = ShimEthDIF("e37")
e38 = ShimEthDIF("e38")
e39 = ShimEthDIF("e39")
e40 = ShimEthDIF("e40")
e41 = ShimEthDIF("e41")
e42 = ShimEthDIF("e42")
e43 = ShimEthDIF("e43")
e44 = ShimEthDIF("e44")

layers = [e1, e2, e3, e4, e5, e6, e7, e8, e9, e10,
          e11, e12, e13, e14, e15, e16, e17, e18, e19, e20,
          e21, e22, e23, e24, e25, e26, e27, e28, e29, e30,
          e31, e32, e33, e34, e35, e36, e37, e38, e39, e40,
          e41, e42, e43, e44]

lisbon = Node("lisbon",
         difs = [n1, e1, e2],
         dif_registrations = {n1: [e1,e2]})

madrid = Node("madrid",
         difs = [n1, e1, e6, e9, e11],
         dif_registrations = {n1 : [e1, e6, e9, e11]})

london = Node("london",
         difs = [n1, e2, e3, e4, e5],
         dif_registrations = {n1 : [e2, e3, e4, e5]})

dublin = Node("dublin",
         difs = [n1, e3],
         dif_registrations = {n1 : [e3]})

paris = Node("paris",
         difs = [n1, e4, e6, e7, e8],
         dif_registrations = {n1 : [e4, e6, e7, e8]})

brussels = Node("brussels",
         difs = [n1, e5, e12],
         dif_registrations = {n1 : [e5, e12]})

luxemburg = Node("luxemburg",
         difs = [n1, e7, e16],
         dif_registrations = {n1 : [e7, e16]})

bern = Node("bern",
         difs = [n1, e8, e9, e10, e15],
         dif_registrations = {n1 : [e8, e9, e10, e15]})

roma = Node("roma",
         difs = [n1, e10, e11, e13, e34, e28],
         dif_registrations = {n1 : [e10, e11, e13, e34, e28]})

amsterdam = Node("amsterdam",
         difs = [n1, e12, e14, e17, e18],
         dif_registrations = {n1 : [e12, e14, e17, e18]})

valleta = Node("valleta",
         difs = [n1, e13, e14],
         dif_registrations = {n1 : [e13, e14]})

berlin = Node("berlin",
         difs = [n1, e15, e16, e17, e30, e31],
         dif_registrations = {n1 : [e15, e16, e17, e30, e31]})

copenhagen = Node("copenhagen",
         difs = [n1, e18, e20, e22, e24, e19],
         dif_registrations = {n1 : [e18, e20, e22, e24, e19]})

oslo = Node("oslo",
         difs = [n1, e20, e21],
         dif_registrations = {n1 : [e20, e21]})

stockholm = Node("stockholm",
         difs = [n1, e21, e22],
         dif_registrations = {n1 : [e21, e22]})

tallin = Node("tallin",
         difs = [n1, e24, e25],
         dif_registrations = {n1 : [e24, e25]})

riga = Node("riga",
         difs = [n1, e25, e26],
         dif_registrations = {n1 : [e25, e26]})

vilnius = Node("vilnius",
         difs = [n1, e26, e27],
         dif_registrations = {n1 : [e26, e27]})

warsaw = Node("warsaw",
         difs = [n1, e27, e29],
         dif_registrations = {n1 : [e27, e29]})

praha = Node("praha",
         difs = [n1, e29, e30, e32],
         dif_registrations = {n1 : [e29, e30, e32]})

viena = Node("viena",
         difs = [n1, e32, e33, e35, e39, e28],
         dif_registrations = {n1 : [e32, e33, e35, e39, e28]})

budapest = Node("budapest",
         difs = [n1, e33, e37, e38, e43],
         dif_registrations = {n1 : [e33, e37, e38, e43]})

athens = Node("athens",
         difs = [n1, e39, e40, e44],
         dif_registrations = {n1 : [e39, e40, e44]})

ljubljana = Node("ljubljana",
         difs = [n1, e35, e36],
         dif_registrations = {n1 : [e35, e36]})

zagreb = Node("zagreb",
         difs = [n1, e36, e37],
         dif_registrations = {n1 : [e36, e37]})

sofia = Node("sofia",
         difs = [n1, e38, e40, e42, e23],
         dif_registrations = {n1 : [e38, e40, e42, e23]})

bucharest = Node("bucharest",
         difs = [n1, e42, e43, e41],
         dif_registrations = {n1 : [e42, e43, e41]})

nicosia = Node("nicosia",
         difs = [n1, e44, e34],
         dif_registrations = {n1 : [e44, e34]})

ankara = Node("ankara",
         difs = [n1, e23, e41],
         dif_registrations = {n1 : [e23, e41]})

moscow = Node("moscow",
         difs = [n1, e31, e19],
         dif_registrations = {n1 : [e31, e19]})

tb = jfed.Testbed(exp_name = 'geant8',
                  cert_file = '/home/sander/cert.pem',
                  authority = 'wall1.ilabt.iminds.be',
#                  authority = 'exogeni.net:umassvmsite',
                  username = 'sander',
                  exp_hours = '200')

nodes = [lisbon, madrid, london, paris, dublin,
         brussels, luxemburg, bern, roma, amsterdam,
         valleta, berlin, copenhagen, oslo, stockholm,
         tallin, riga, vilnius, warsaw, praha, viena,
         budapest, athens, ljubljana, zagreb, sofia,
         bucharest, nicosia, ankara, moscow]

exp = our.Experiment(tb, nodes=nodes,
                     git_repo='https://bitbucket.org/sandervrijders/ouroboros.git',
                     git_branch='rand3')


with ExperimentManager(exp, swap_out_strategy=AUTO_SWAPOUT):
    exp.swap_in()
    exp.install_prototype()
    exp.bootstrap_prototype()
    for layer in layers:
        for i in range(1,4):

            duration = 1220

            sb = StoryBoard(duration + 30)

            sb.set_experiment(exp)

            for node in nodes:
                s = Server(
                    "oping",
                    options="-l",
                    arrival_rate=0.01,
                    mean_duration=duration,
                    s_id='oping_' + node.name,
                    difs=n1
                )

                sb.add_server_on_node(s, node)

            # Generate pings to check rtt, latency and drops
            couples = {(a, b) for i, a in enumerate(nodes) for b in nodes[i+1:]}

            time = 30

            for node1, node2 in couples:

                tag12 = '%s_%s' % (node1.name, node2.name)
                tag21 = '%s_%s' % (node2.name, node1.name)

                c_opt_base = "-d 940000 -i 100ms -D -n "

                client12 = Client(
                    "oping",
                    options=c_opt_base + 'oping_' + node2.name,
                    shutdown="",
                    c_id='__rpc_' + tag12
                )

                client21 = Client(
                    "oping",
                    options=c_opt_base + 'oping_' + node1.name,
                    shutdown="",
                    c_id='__rpc_' + tag21
                )

                sb.schedule_action(
                    sb.run_client,
                    args=[client12, duration, node1, '__rpp_' + tag12],
                    c_time=time,
                    ev_id='ev_run_rp_' + tag12
                )

                sb.schedule_action(
                    sb.run_client,
                    args=[client21, duration, node2, '__rpp_' + tag21],
                    c_time=time,
                    ev_id='ev_run_rp_' + tag21
                )

                time += 0.4

            sb.schedule_link_down(560, layer)
            sb.schedule_link_up(750, layer)

            prefix = layer.name + '_' + str(i)

            sb.schedule_export_lsdb_total(390, prefix + '_lsdb.csv', n1)
            sb.schedule_export_dif_bandwidth(410, prefix + '_1.csv', n1)
            sb.schedule_export_dif_bandwidth(510, prefix + '_2.csv', n1)
            sb.schedule_export_dif_bandwidth(610, prefix + '_3.csv', n1)
            sb.schedule_export_dif_bandwidth(710, prefix + '_4.csv', n1)

            sb.start()
