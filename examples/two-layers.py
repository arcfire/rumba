#!/usr/bin/env python

# An example script using the rumba package

from rumba.model import *
from rumba.storyboard import *

# import testbed plugins
import rumba.testbeds.emulab as emulab
import rumba.testbeds.jfed as jfed
import rumba.testbeds.local as local
import rumba.testbeds.qemu as qemu

# import prototype plugins
import rumba.prototypes.ouroboros as our
import rumba.prototypes.rlite as rl
import rumba.prototypes.irati as irati

import rumba.log as log
from rumba.utils import ExperimentManager

log.set_logging_level('DEBUG')


n1 = NormalDIF("n1")
n2 = NormalDIF("n2")
n3 = NormalDIF("n3")
n4 = NormalDIF("n4")

e1 = ShimEthDIF("e1")
e2 = ShimEthDIF("e2")
e3 = ShimEthDIF("e3")

a = Node("a",
         difs = [n3, n4, n1, e1],
         dif_registrations = {n4: [n1], n3: [n1], n1 : [e1]})

b = Node("b",
         difs = [n1, e1, e2],
         dif_registrations = {n1 : [e1, e2]})

c = Node("c",
         difs = [n3, n4, n1, n2, e2, e3],
         dif_registrations = {n4: [n1], n3: [n1, n2], n1 : [e2], n2: [e3]})

d = Node("d",
         difs = [n3, n2, e3],
         dif_registrations = {n3: [n2], n2 : [e3]})

tb = qemu.Testbed(exp_name = "twolayers",
                  username = "root",
                  password = "root")

exp = rl.Experiment(tb, nodes = [a, b, c, d])

print(exp)

with ExperimentManager(exp):
    exp.swap_in()
    exp.bootstrap_prototype()
    sb = StoryBoard(experiment=exp, duration=15, servers=[])
    sb.schedule_command(7.5, a, 'echo "7.5 secs in. We are at $(hostname)"')
    sb.schedule_command(12, b, 'echo "12 secs in. We are at $(hostname)"')
    sb.start()
