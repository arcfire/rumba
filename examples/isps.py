#!/usr/bin/env python

from rumba.model import *
import rumba.testbeds.jfed as jfed
import rumba.prototypes.ouroboros as our

import rumba.log as log

from rumba.storyboard import *

from rumba.utils import *

import argparse
import time

log.set_logging_level('DEBUG')

n0 = NormalDIF("n0")

n1 = NormalDIF("n1")

n2 = NormalDIF("n2")
n2.add_policy("routing", "lfa")
n2.add_policy("pff", "alternate")

e0 = ShimEthDIF("e0")
e1 = ShimEthDIF("e1")
e2 = ShimEthDIF("e2")
e3 = ShimEthDIF("e3")
e4 = ShimEthDIF("e4")
e5 = ShimEthDIF("e5")
e6 = ShimEthDIF("e6")
e7 = ShimEthDIF("e7")
e8 = ShimEthDIF("e8")

es = [e0, e1, e2, e3, e4, e5, e6, e7, e8]

for e in es:
    e.set_delay(5)

end0 = Node("end0",
            difs = [e0, e8, n2],
            dif_registrations = {n2: [e0, e8]})

end1 = Node("end1",
            difs = [e4, e5, n2],
            dif_registrations = {n2: [e4, e5]})

isp10 = Node("isp10",
             difs = [e0, e1, n2, n1],
             dif_registrations = {n2: [e0, n1], n1: [e1]})

isp11 = Node("isp11",
             difs = [e1, e2, n1],
             dif_registrations = {n1: [e1, e2]})

isp12 = Node("isp12",
             difs = [e2, e3, n1],
             dif_registrations = {n1: [e2, e3]})

isp13 = Node("isp13",
             difs = [e3, e4, n2, n1],
             dif_registrations = {n2: [e4, n1], n1: [e3]})

isp00 = Node("isp00",
             difs = [e8, e7, n2, n0],
             dif_registrations = {n2: [e8, n0], n0: [e7]})

isp01 = Node("isp01",
             difs = [e7, e6, n0],
             dif_registrations = {n0: [e7, e6]})

isp02 = Node("isp02",
             difs = [e6, e5, n2, n0],
             dif_registrations = {n2: [e5, n0], n0: [e6]})

tb = jfed.Testbed(exp_name = 'case3',
                  cert_file = '/home/sander/cert.pem',
                  username = 'sander',
                  exp_hours = '1')

nodes = [end0, end1, isp10, isp11, isp12, isp13, isp00, isp01, isp02]

exp = our.Experiment(tb, nodes=nodes,
                     git_repo='https://bitbucket.org/sandervrijders/ouroboros.git',
                     git_branch='rand3')

duration = 120

sb = StoryBoard(duration + 7)

sb.set_experiment(exp)

s = Server(
    "oping",
    options="-l",
    arrival_rate=0.01,
    mean_duration=duration + 4,
    s_id='oping',
    difs=n2
)

sb.add_server_on_node(s, end1)

client = Client(
    "oping",
    options='-c 116000 -i 1 -D -n oping',
    shutdown="",
    c_id='__rpc_'
)

sb.schedule_action(
    sb.run_client,
    args=[client, duration, end0, '__rpp_'],
    c_time=8,
    ev_id='ev_run_rp'
)

sb.schedule_command(66, end1, 'irm i d n e5.end1 &')
sb.schedule_command(66, end0, 'irm i d n e8.end0 &')

with ExperimentManager(exp, swap_out_strategy=AUTO_SWAPOUT):
    exp.swap_in()
    exp.install_prototype()
    exp.bootstrap_prototype()
    sb.start()
    sb.fetch_logs()
