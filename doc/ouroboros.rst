Ouroboros
=============

`Ouroboros <https://ouroboros.ilabt.imec.be/>`_ is a user-space
implementation with a focus on portability. It is written in C89 and
works on any POSIX.1-2001 enabled system.

.. automodule:: rumba.prototypes.ouroboros
   :member-order: bysource
   :show-inheritance:
   :inherited-members:
