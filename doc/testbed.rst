Testbed plugins
-------------------------

Rumba provides a generic Testbed class that specific implementations
can derive from. All testbeds have to re-implement the functions
provided by the base class.

.. autoclass:: rumba.model.Testbed

Specific implementations of the Testbed class:

.. toctree::
    docker
    emulab
    jfed
    local
    qemu
