Defining the recursive network
==============================

Experimenters can define their recursive network by using the building
blocks listed below.

.. autoclass:: rumba.model.Node

.. autoclass:: rumba.model.DIF

.. autoclass:: rumba.model.NormalDIF

.. autoclass:: rumba.model.ShimUDPDIF

.. autoclass:: rumba.model.ShimEthDIF

.. autoclass:: rumba.model.Distribution

.. autoclass:: rumba.model.Delay

.. autoclass:: rumba.model.Loss

.. autoclass:: rumba.model.LinkQuality
