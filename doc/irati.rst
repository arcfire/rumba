IRATI
=============

`IRATI <https://github.com/IRATI/stack>`_ is an open source
implementation of the RINA architecture targeted at the OS/Linux
system, initially developed by the FP7-IRATI project.

.. automodule:: rumba.prototypes.irati
    :member-order: bysource
    :show-inheritance:
    :inherited-members:
