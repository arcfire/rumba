Running Rumba interactively
*****************************

As Rumba is a Python framework, it is possible to run experiments
interactively. In order to do so, simply run Python in a terminal and
import the script. An example is given below.

First start by creating a new Python file (file.py) wherein the
Recursive Network is defined, the testbed and the experiment. In the
example, let's have 2 nodes with an Ethernet layer between them and
run that on the jFed testbed with a custom install of the Ouroboros
prototype. ::

  #!/usr/bin/env python

  from rumba.model import *
  import rumba.testbeds.jfed as jfed
  import rumba.prototypes.ouroboros as our

  e1 = ShimEthDIF("e1")

  a = Node("a", difs = [e1])

  b = Node("b", difs = [e1])

  tb = jfed.Testbed(exp_name = "test",
                    cert_file = "/location/of/cert.pem",
                    username = "username")

  exp = our.Experiment(tb,
                       nodes = [a, b],
                       git_repo='/location/of/custom/ouroboros/git/repo')

Now that we have the Python file with our experiment defined, we can
run it interactively. ::

  [user@computer ~]$ python
  Python 3.7.2 (default, Dec 29 2018, 21:15:15)
  [GCC 8.2.1 20181127] on linux
  Type "help", "copyright", "credits" or "license" for more information.
  >>> from file import *

Once the experiment is loaded, it is possible to execute functions. ::

  >>> exp.swap_in()
  >>> exp.install_prototype()
  >>> exp.bootstrap_prototype()
  >>> exp.terminate_prototype()
  >>> exp.swap_out()

A common use case would be to install the prototype, bootstrap it,
debug the experiment, terminate the prototype, install it again, ...
