Storyboard
=============

Experimenters can create a Storyboard to emulate real network traffic
to be run after the recursive network is up and running.

.. automodule:: rumba.storyboard
    :member-order: bysource
