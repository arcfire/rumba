Workflow
************************

1. Define the network graph, creating instances of model.Node and
   model.DIF classes. Experiments can import rumba.model to obtain all
   Rumba classes needed to run an experiment.

2. Create an instance of a specific model.Testbed class (QEMU, Docker,
   Local, Emulab or jFed).

3. Create an instance of the selected prototype.Experiment class
   (Ouroboros, rlite, IRATI), passing the testbed instance and a list
   of Node instances.

  * At the end of the base Experiment constructor, the
    generate function is called to generate information about
    per-node IPCPs, registrations and enrollment, ready to be
    used by the prototype plugins.

4. Call methods on the prototype.Experiment instance:

  * swap_in() swaps the experiment in on the testbed, and fills in
    the missing information in the model.

  * install_prototype() installs the chosen prototype on the testbed
    (if necessary). Currently an Ubuntu image is assumed.

  * bootstrap_prototype() calls a prototype-specific setup function,
    to create the required IPCPs, perform registrations,
    enrollments, etc.

  * swap_out() swaps the experiment out of the testbed.

5. Optionally run a Storyboard to emulate real network traffic.


Accessing nodes after swap-in
-----------------------------

To access a node once the experiment is swapped in, use the following
command (in the same terminal where ssh-agent was run in case of jFed): ::

    $ rumba-access $NODE_NAME

Where $NODE_NAME is the name of the node to access.
