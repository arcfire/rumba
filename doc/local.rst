Local
=============

.. automodule:: rumba.testbeds.local
    :member-order: bysource
    :show-inheritance:
    :inherited-members:
