Utils
=============

.. autoclass:: rumba.utils.ExperimentManager
   :member-order: bysource

Example usage of the class:

.. code-block:: python

   from rumba.utils import ExperimentManager, PROMPT_SWAPOUT

   with ExperimentManager(exp, swap_out_strategy=PROMPT_SWAPOUT):
       exp.swap_in()
       exp.bootstrap_prototype()
