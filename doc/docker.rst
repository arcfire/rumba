Docker
=============

`Docker <https://www.docker.com/>`_ is a container runtime
environment.

To use the Docker testbed the Docker software needs to be installed,
see `Install Docker <https://docs.docker.com/install/>`_ and complete
`Post-installation steps for Linux
<https://docs.docker.com/install/linux/linux-postinstall/>`_.

.. automodule:: rumba.testbeds.dockertb
    :member-order: bysource
    :show-inheritance:
    :inherited-members:
